#! /usr/bin/env bash

trap 'exit 1' ERR

v=`cat libxsd-frontend/version.txt`

echo "packaging libxsd-frontend-$v"
echo "EVERYTHING MUST BE COMMITTED!"

# prepare libxsd-frontend-x.y.z
#
rm -rf libxsd-frontend-$v
mkdir libxsd-frontend-$v
cd libxsd-frontend
git archive legacy | tar -x -C ../libxsd-frontend-$v
cd ..
rm -f libxsd-frontend-$v/.gitignore libxsd-frontend-$v/.gitattributes

# Copy generated source files.
#
cd libxsd-frontend/xsd-frontend
make

files="semantic-graph/fundamental.?xx"

for f in $files; do
  rsync -aq $f ../../libxsd-frontend-$v/xsd-frontend/$f
  touch ../../libxsd-frontend-$v/xsd-frontend/$f
done

cd ../..

# Package
#
tar cfj libxsd-frontend-$v.tar.bz2 libxsd-frontend-$v
sha1sum libxsd-frontend-$v.tar.bz2 >libxsd-frontend-$v.tar.bz2.sha1
